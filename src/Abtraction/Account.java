package Abtraction;

public interface Account {
    void deposit(double amt);
    void  withdraw(double amt);
    void  checkBalance();

}
