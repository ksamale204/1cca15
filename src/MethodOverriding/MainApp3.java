package MethodOverriding;

import java.util.Scanner;

public class MainApp3 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("ENTER  AMOUNT ");
        double Amt= sc1.nextDouble();
        System.out.println("ENTER NUMBER OF YEAR OF DEPOSITED AMOUNT");
        double duration= sc1.nextDouble();
        System.out.println("SELECT YOUR BANK");
        System.out.println("1:AXIS\n2:SBI\n3:HDFC");
        int choice= sc1.nextInt();
        if (choice==1){
            Axis a1=new Axis();
            a1.interest(Amt,duration);
        } else if (choice==2) {
            SBI s1=new SBI();
            s1.interest(Amt,duration);

        } else if (choice==3) {
            HDFC h1=new HDFC();
            h1.interest(Amt,duration);

        }else{
            System.out.println("INVALID CHOICE");
        }

    }
}
