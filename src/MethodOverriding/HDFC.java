package MethodOverriding;

public class HDFC extends Bank {
    @Override
    void interest(double Amt, double duration) {
        double interest=(Amt*5.5*duration)/100;
        System.out.println("HDFC Bank interest is="+interest);
    }
}
