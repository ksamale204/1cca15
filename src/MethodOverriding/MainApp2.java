package MethodOverriding;

import java.util.Scanner;

public class MainApp2 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("ENTER FEES OF QSPIDERS");
        double fees= sc1.nextDouble();
        System.out.println("SELECT PAYMENT METHOD");
        System.out.println("1:oneshot\n2:installment");
        int choice= sc1.nextInt();
        if (choice == 1) {

            Onshot q1=new Onshot();
            q1.payment(fees);
        } else if (choice==2) {
            Installment i1=new Installment();
            i1.payment(fees);

        }
        else {
            System.out.println("invalid choice");
        }
    }
}
