package Casting;

public class Demo4 {
    public static void main(String[] args) {
        short s1= (short) 1234569;
        System.out.println(s1);
        long l1= 4954656567787L;
        int x1= (int) l1;
        System.out.println(x1);
    }
}
