package Casting.Upcasting;

public class MaharashtraExpress extends Train {
    double[] cost = {300, 350, 400};


    @Override
    void BookTicket(int qty, int routeChoice) {

        boolean found = false;
        for (int a = 0; a < routes.length; a++) {
            if (routeChoice == a) {

                double total = qty * cost[a];
                System.out.println("TOTAL COST IS:" + total);
                found = true;
            }

        }

        if (!found) {
            System.out.println("INVALID ROUTE");
        }
    }
}