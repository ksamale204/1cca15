package Casting.Upcasting;

import java.util.Scanner;

public class MainApp6 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("SELECT ROUTE");
        System.out.println("0:PUNE TO PACHORA ");
        System.out.println("1:PUNE TO CHOPADA");
        System.out.println("2:PUNE TO CHALISGOAN");
        int routeChoice= sc1.nextInt();
        System.out.println("SELECT TRAVELS");
        System.out.println("1:MAMTA TRAVELS\n2:BALAJI TRAVELS\n:ASHA TRAVELS");
        int choice= sc1.nextInt();
        System.out.println("ENTER NO OF TICKETS");
        int tickets= sc1.nextInt();
        Travels t1=null;

        if (choice==1){
            t1=new MamtaTravels();
        } else if (choice==2) {
            t1=new BalajiTravels();

        } else if (choice==3) {
            t1=new AshaTravels();

        }
        t1.BookTicket(tickets,routeChoice);
    }
}
