package Casting.Upcasting;

public class OD extends CAR{
    @Override
    void getType() {
        System.out.println("CAR COMPANY IS OD");
        System.out.println("SPEED OD IS 350kmph");
    }

    @Override
    void calculateBill(int qty, double price) {
        double total=qty*price;
        //8%discount
        double FinalAmt=total-total*0.08;
        System.out.println("FINAL AMOUNT IS:"+FinalAmt);
    }
}
