package Casting.Upcasting;

import java.util.Scanner;

public class MainApp7 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("SELECT ROUTE");

        System.out.println("0:PUNE TO JALGOAN\n1:PUNE TO BHUSAVAL\n2:PUNE TO PACHORA");
        int routeChoice= sc1.nextInt();
        System.out.println("SELECT EXPRESS");
        System.out.println("1:MAHARASHTRA EXPRESS\n 2: KAMAYANI EXPRESS");
        int exp= sc1.nextInt();
        System.out.println("ENTER TICKET TYPE");
        System.out.println("1:GENERAL\n2:RESERVATION");
        int choice= sc1.nextInt();
        System.out.println("ENTER NO OF TICKETS");
        int qty= sc1.nextInt();
        Train t1=null;
        if (exp==1) {
            if (choice == 1)
                t1 = new General();
            else if (choice == 2)
                t1 = new Reservation();

            else
                System.out.println("INVALID CHOICE");
        }
        else if (exp==2){
                if (choice==1)
                    t1=new General1();
                 else if (choice==2)
                    t1=new Reservation1();

                else
                    System.out.println("INVALID CHOICE");


        }
        t1.BookTicket(qty,routeChoice);
    }
}
