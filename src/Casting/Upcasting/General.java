package Casting.Upcasting;

public class General extends MaharashtraExpress {
    double[] cost = {200, 250, 150};
    int availableTickets = 50;

    @Override
    void BookTicket(int qty, int routeChoice) {
        boolean found = false;
        for (int a = 0; a < routes.length; a++) {
            if (routeChoice == a) {

                if (qty <= availableTickets) {
                    availableTickets -= qty;

                    double total = qty * cost[a];
                    System.out.println("TOTAL COST IS:" + total);
                    found = true;
                } else {
                    System.out.println("TICKET NOT AVAILABLE");

                }
            }

        }
        if (!found) {
                        System.out.println("INVALID ROUTE");
            }
        }
    }
