package Casting.Upcasting;

public class projector extends Machine{
    @Override
    void getType() {
        System.out.println("MACHINE TYPE IS PROJECTOR");
    }

    @Override
    void calculateBill(int qty, double price) {
        double total =qty*price;
        double finalAmt=total+total*0.1;
        System.out.println("FINAL AMOUNT IS:"+finalAmt);
    }
}
