package Casting.Upcasting;

import javax.jws.soap.SOAPBinding;

public class SwiftDesire extends CAR{
    @Override
    void getType() {
        System.out.println("CAR TYPE IS SWIFT DZIRE");
        System.out.println("SPEED OF SWIFT DZIRE IS 170KMPH");
    }

    @Override
    void calculateBill(int qty, double price) {
        double total=qty*price;
        double FinalAmt=total-total*0.07;
        System.out.println("FINAL AMOUNT IS "+FinalAmt);
    }
}
