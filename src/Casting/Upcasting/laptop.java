package Casting.Upcasting;

public class laptop extends Machine{
    void getType(){
        System.out.println("MACHINE TYPE IS LAPTOP");
    }
    void calculateBill(int qty, double price){
        //15% GST added
        double total=qty*price;
        double finalAmt=total+total*0.15;
        System.out.println("FINAL AMOUNT IS:"+finalAmt);
    }
}
