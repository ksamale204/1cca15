package Casting.Upcasting;

import java.util.Scanner;

public class MainApp5 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("select service provider");
        System.out.println("1:AirAsia\n2:Indigo");
        int choice= sc1.nextInt();
        System.out.println("SELECT ROUTE");
        System.out.println("0:PUNE TO DELHI");
        System.out.println("1:MUMBAI TO CHENNAI");
        System.out.println("2:KOLKATA TO BANGALORE");
        int routeChoice= sc1.nextInt();
        System.out.println("ENTER NO OF TICKET");
        int tickets= sc1.nextInt();
        Goibibo g1=null;
        if (choice==1){
            g1=new AirAsia();
        } else if (choice==2) {
            g1=new Indigo();

        }
        g1.BookTicket(tickets , routeChoice);
    }
}
