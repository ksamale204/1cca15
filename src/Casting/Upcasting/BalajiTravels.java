package Casting.Upcasting;

public class BalajiTravels extends Travels {
    double[] cost = {2500.55};

    @Override
    void BookTicket(int qty, int routeChoice) {
        boolean found = false;
        for (int a = 0; a < routes.length; a++) {
            if (routeChoice == a) {
                double total = qty * cost[a];
                //DIWALI OFFER 6%DISCOUNT
                double FinalAmt = total - total * 0.06;
                System.out.println("FINAL AMOUNT IS:" + FinalAmt);
                found = true;
            }
        }
        if (!found) {
            System.out.println("INVALID ROUTE");
        }
    }
}