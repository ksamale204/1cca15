package Casting.Upcasting;

public class Machine {
    void getType(){
        System.out.println("DISPLAY METHOD");
    }
    void calculateBill(int qty,double price) {
        double total = qty * price;
        System.out.println("TOTAL AMOUNT" + total);
    }
}
