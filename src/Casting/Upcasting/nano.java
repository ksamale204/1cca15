package Casting.Upcasting;

public class nano extends CAR{
    @Override
    void getType() {
        System.out.println("CAR COMPANY IS NANO");
        System.out.println("SPEED OF CAR IS:105kmph");
    }

    @Override
    void calculateBill(int qty, double price) {
        double total=qty*price;
        double FinalAmt=total-total*0.02;
        System.out.println("FINAL AMOUNT IS:"+FinalAmt);
    }
}
