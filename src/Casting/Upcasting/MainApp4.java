package Casting.Upcasting;

import java.util.Scanner;

public class MainApp4 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("ENTER QTY");
        int qty= sc1.nextInt();
        System.out.println("ENTER PRICE");
        double price= sc1.nextDouble();
        System.out.println("SELECT CAR COMPANY");
        System.out.println("1:HONDA CITY\n2:OD\n3:NANO\n4:SWIFT DZIRE");
        int choice= sc1.nextInt();
        CAR c1=null;
        if (choice==1){
            c1=new HondaCity();

        } else if (choice==2) {
            c1=new OD();
        } else if (choice==3) {
          c1=new nano();
        } else if (choice==4) {
            c1=new SwiftDesire();
        }else{
            System.out.println("invalid choice");
        }
        c1.getType();
        c1.calculateBill(qty,price);
    }
}
