package MethodOverloading;

import java.util.Scanner;

public class MainApp {
    public static void main(String[] args) {
        Facebook f1 = new Facebook();
        Scanner Sc1 = new Scanner(System.in);
        System.out.println("SELECT SEARCH CRITERIA");
        System.out.println("1:search by Email \n 2:search by contact");
        int choice = Sc1.nextInt();
        if (choice == 1) {
            System.out.println("Enter Email");
            String email = Sc1.next();
            f1.login(email, f1.password);
        } else if (choice == 2)
        {
            System.out.println("enter contact");
            long contact = Sc1.nextLong();

            f1.login(1234567890, "kavita1234");
        }
        else{
            System.out.println("Invalide choice");
        }
    }
}
