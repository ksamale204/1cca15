package DownCasting;

public class mainApp6 {
    public static void main(String[] args) {
        Bike b=new ElectricBike();
        b.getType();
        System.out.println("========================");
        ElectricBike e=(ElectricBike) b;
        e.getType();
        e.batteryInfo();

        }

}
